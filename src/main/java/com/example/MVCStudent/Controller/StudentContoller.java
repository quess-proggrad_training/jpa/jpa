package com.example.MVCStudent.Controller;

import com.example.MVCStudent.Model.StudentModel;
import com.example.MVCStudent.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentContoller {
   @Autowired
    StudentService studentServiceObject;
   @PostMapping ("/addstudent")
   public String AddStudent(@RequestBody StudentModel studentModelObject)
   {

       String str= studentServiceObject.addStudent(studentModelObject);
       return str;

   }

   @GetMapping("/getstudent")
   public List<StudentModel> fetchStudent()
   {
       return studentServiceObject.getstudent();
   }

   @GetMapping("/studentbyrollno/{rollno}")
   public List<StudentModel> getStudentByRollno(@PathVariable int rollno )
   {
            return studentServiceObject.getstudentbyrollno(rollno);
   }

   @GetMapping("/studentbyname/{name}")
   public List<StudentModel> getStudentByname(@PathVariable String name)
   {
       return studentServiceObject.getstudentbyname(name);
   }

   @DeleteMapping("/deletebyrollno/{rollno}")
   public void deleteStudentByRollno(@PathVariable int rollno)
   {
       studentServiceObject.deletestudent(rollno);
   }

   @PutMapping("/updatestudent/{rollno}")
   public void update(@RequestBody StudentModel studentModelObj, @PathVariable int rollno)
   {
       studentServiceObject.updateStudent(studentModelObj,rollno);
   }

}
