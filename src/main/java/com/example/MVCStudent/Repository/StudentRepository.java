package com.example.MVCStudent.Repository;

import com.example.MVCStudent.Model.StudentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<StudentModel,Integer> {


    List<StudentModel> findByRollno(int rollno);

    List<StudentModel> findByName(String name);


}
