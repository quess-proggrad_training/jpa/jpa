package com.example.MVCStudent.Service;

import com.example.MVCStudent.Model.StudentModel;
import com.example.MVCStudent.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {
       @Autowired
    StudentRepository studentRepositoryObject;


    public String addStudent(StudentModel studentModelObj) {
        studentRepositoryObject.save(studentModelObj);
        return "Student Added";
    }

    public List<StudentModel> getstudent() {
       return studentRepositoryObject.findAll();
    }

    public List<StudentModel> getstudentbyrollno(int rollno) {
        return studentRepositoryObject.findByRollno(rollno);
    }

    public List<StudentModel> getstudentbyname(String name) {
        return studentRepositoryObject.findByName(name);
    }

    public void deletestudent(int rollno) {
        studentRepositoryObject.deleteById(rollno);
    }

    public void updateStudent(StudentModel studentModelObj, int rollno) {
        StudentModel fetchedstudentObj =studentRepositoryObject.findById(rollno).get();
        if(fetchedstudentObj!=null)
        {
            studentRepositoryObject.delete(fetchedstudentObj);
            studentRepositoryObject.save(studentModelObj);
        }
    }
}
