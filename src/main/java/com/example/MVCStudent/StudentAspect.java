package com.example.MVCStudent;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class StudentAspect {
  @Before("execution(* com.example.MVCStudent.Controller.StudentContoller.Control())")
    public void before()
    {
        System.out.println("Process Started");
    }
    @After("execution(* com.example.MVCStudent.Controller.StudentContoller.Control())")
    public void after()
    {
        System.out.println("Process Finished");
    }
}
