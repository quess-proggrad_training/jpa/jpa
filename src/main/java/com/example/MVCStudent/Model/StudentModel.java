package com.example.MVCStudent.Model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class StudentModel {
    @Id
    private int rollno;
    private String name;

        private int Std;

    public StudentModel() {
    }

    public StudentModel(String name, int rollno, int std) {
        this.name = name;
        this.rollno = rollno;
        this.Std = std;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    public int getRollno() {
        return rollno;
    }

    public void setRollno(int rollno) {
        this.rollno = rollno;
    }

    public int getStd() {
        return Std;
    }

    public void setStd(int std) {
        Std = std;
    }


}

